/* user and group to drop privileges to */
static const char *user  = "barbaross";
static const char *group = "barbaross";

static const char *colorname[NUMCOLS] = {
	[BACKGROUND] =   "#1c1c1c",     /* after initialization */
	[INIT] =   "#dfdfaf",     /* after initialization */
	[INPUT] =  "#dfdfaf",   /* during input */
	[FAILED] = "#af5f5f",   /* wrong password */
	[CAPS] = "af5f5f",         /* CapsLock on */
	[PAM] =    "#dfdfaf",   /* waiting for PAM */
};

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "bgcolor", 	STRING, &colorname[BACKGROUND] },
		{ "initcolor",       STRING,  &colorname[INIT] },
		{ "inputcolor",       STRING,  &colorname[INPUT] },
		{ "failcolor",       STRING,  &colorname[FAILED] },
		{ "capscolor",       STRING,  &colorname[CAPS] },
		{ "pamcolor", 			STRING, &colorname[PAM] },
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* length of time (seconds) until */
static const int timeoffset = 300;

/* should [command] be run only once? */
static const int runonce = 1;

/* command to be run after [time] has passed */
static const char *command = "sudo zzz -H";

/* PAM service that's used for authentication */
static const char* pam_service = "system-auth";

/* insert grid pattern with scale 1:1, the size can be changed with logosize */
static const int logosize = 25;
static const int logow = 23;	/* grid width and height for right center alignment*/
static const int logoh = 12;

static XRectangle rectangles[24] = {
	/* x	y	w	h */
	{ 1, 0, 1, 10 },
	{ 2, 0, 1, 1 },
	{ 2, 4, 1, 1 },
	{ 3, 1, 1, 1 },
	{ 3, 3, 1, 1 },
	{ 4, 2, 1, 1 },
	{ 5, 1, 1, 1 },
	{ 5, 3, 1, 1 },
	{ 6, 0, 1, 1 },
	{ 6, 4, 1, 1 },
	{ 7, 0, 1, 10 },
	{ 9, 0, 1, 10},
	{ 10, 0, 1, 1 },
	{ 11, 1, 1, 1 },
	{ 12, 2, 1, 1 },
	{ 13, 1, 1, 1 },
	{ 14, 0, 1, 1 },
	{ 15, 0, 1, 10 },
	{ 17, 0, 1, 10 },
	{ 18, 0, 1, 1 },
	{ 19, 1, 1, 1},
	{ 20, 2, 1, 1 },
	{ 21, 3, 1, 7 },
	{ 0, 11, 23, 1 },
};

/* number of failed password attempts until failcommand is executed.
   Set to 0 to disable */
static const int failcount = 5;

/* command to be executed after [failcount] failed password attempts */
static const char *failcommand = "sudo ZZZ";

/* allow control key to trigger fail on clear */
static const int controlkeyclear = 1;
